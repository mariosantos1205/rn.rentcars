import React, { useEffect, useState } from 'react';
import { View, Image, Text, TextInput, TouchableOpacity, StyleSheet, AsyncStorage, KeyboardAvoidingView, Platform } from 'react-native';
import logo from '../../assets/airbnb_logo.png'

const Login = ({ navigation }) => {
  const [ techs, setTechs ] = useState('')
  const [ user, setUser ] = useState('')

  useEffect(() => {        
    // navigation.navigate('MyList')
  }, [])

  async function handleSubmmit() {    
      navigation.navigate('MyList')
  }

  return (  
      <KeyboardAvoidingView enabled={ Platform.OS === 'ios' } behavior="padding" style={ styles.container }>
          <Image source={ logo } style={ styles.logo } />
          <View style={ styles.form }>
            <Text style={ styles.label }>SEU E-MAIL *</Text>
            <TextInput
            style={ styles.input }
            placeholder="Seu e-mail"
            placeholderTextColor="#999"
            keyboardType="email-address"
            autoCapitalize="none"
            onChange={ setUser }
            autoCorrect={ false } />

            <Text style={ styles.label }>TECNOLOGIAS *</Text>
            <TextInput
            style={ styles.input }
            placeholder="Tecnologias de interesse"
            placeholderTextColor="#999"            
             autoCapitalize="none"
            onChange={ setTechs }
            autoCorrect={ false } />
            
            <TouchableOpacity style={ styles.button } onPress={ handleSubmmit }>                
                <Text style={ styles.buttonText }>Encontrar spots</Text>
            </TouchableOpacity>
          </View>
      </KeyboardAvoidingView>
  )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },     
    logo: {
        height: 150,
        width: 200,
        resizeMode: 'contain'
    },
    label: {
        fontWeight: 'bold',
        color: '#444',
        marginBottom: 8
    },
    form: {
        alignSelf: 'stretch',
        paddingHorizontal: 30,
        // marginTop: 30
    },
    input: {
        borderWidth: 1,
        borderColor: '#ddd',
        paddingHorizontal: 20,
        fontSize: 16,
        color: '#444',
        height: 44,
        marginBottom: 20,
        borderRadius: 5
    },
    button: {
        height: 42,
        backgroundColor: '#f05a5b',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        borderRadius: 5
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16
    }
})

export default Login;
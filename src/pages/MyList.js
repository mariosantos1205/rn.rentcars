import React, {  } from 'react'
import { Image, SafeAreaView, ScrollView, StyleSheet } from 'react-native'
import logo from '../../assets/airbnb_logo.png'
import CarsList from '../components/CarsList'
import { Categories } from '../data'

const MyList = () => {  

  return (
    <SafeAreaView style={ styles.container }>
      <Image source={ logo } style={ styles.logo } />
      <ScrollView>
        {Categories.map(item => (
            <CarsList key={ item } category={ item } />
          ))}
      </ScrollView>        
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  logo: {
    height: 100,
    width: 150,
    alignSelf: 'center',
    resizeMode: 'contain'
  }
})

export default MyList
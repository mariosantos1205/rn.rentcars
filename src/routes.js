import { createAppContainer, createSwitchNavigator } from 'react-navigation'

import Login from './pages/Login'
import Books from './pages/Books'
import MyList from './pages/MyList'

const Routes = createAppContainer(
    createSwitchNavigator({
        Login,
        Books,
        MyList
    })
)

export default Routes
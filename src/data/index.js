export const Cars = [
    {
        id: 1,
        category: 'populares',        
        thumbnail: 'foto-1.png',
        company: 'Ford',
        price: 50.00,
        thumbnail_url: 'https://lh6.googleusercontent.com/O782g9zClQ-1q0sndK5gYeIiHUpDLPmVPJ-pspAQe6tzIZHzQihbRs10SYA14C2VrFQGZfPOg9Pf0smWr0ZQRyX1-65dhVf8vb0FurM5Si2U6CAgf5qj-BKjkKOTnnNOxEh2gejd',
        name: 'joaquim'
    },
    {
        id: 2,
        category: 'populares',        
        thumbnail: 'foto-1.png',
        company: 'Volks',
        price: 50.00,
        thumbnail_url: 'https://conectaconsorciovw.com.br/blog/wp-content/uploads/2015/11/Fox-Highline-1-768x419.jpg',
        name: 'joaquim'
    },
    {
        id: 3,
        category: 'populares',
        thumbnail: 'foto-1.png',
        company: 'Volks',
        price: 50.00,
        thumbnail_url: 'https://start.youse.com.br/wp-content/uploads/2020/01/VW-T-Cross-SUV-mais-vendido.jpg',
        name: 'joaquim'
    },
    {
        id: 4,
        category: 'populares',
        thumbnail: 'foto-1.png',
        company: 'Hyundai',
        price: 50.00,
        thumbnail_url: 'https://blog.123carros.com.br/wp-content/uploads/2018/01/Hatch-620x445.jpg',
        name: 'joaquim'
    },
    {
        id: 5,
        category: 'populares',
        thumbnail: 'foto-1.png',
        company: 'Chevrolet',
        price: 50.00,
        thumbnail_url: 'https://www.chevrolet.com.br/content/dam/chevrolet/mercosur/brazil/portuguese/index/cars/cars-subcontent/04-images/novo-onix-branco-summit.png?imwidth=960',
        name: 'joaquim'
    },
    {
        category: 'esportivos',
        id: 6,
        thumbnail: 'foto-1.png',
        company: 'Ferrari',
        price: 50.00,
        thumbnail_url: 'https://revistacarro.com.br/wp-content/uploads/2020/01/ferrari-458-spider_1280x768.jpg',
        name: 'joaquim'
    },
    {
        category: 'esportivos',
        id: 7,
        thumbnail: 'foto-1.png',
        company: 'Chevrolet',
        price: 50.00,
        thumbnail_url: 'https://www.portalautoshopping.com.br/blog/wp-content/uploads/2019/07/confira-7-musicas-brasileiras-que-falam-sobre-carros.jpeg',
        name: 'joaquim'
    },
    {
        category: 'esportivos',
        id: 8,
        thumbnail: 'foto-1.png',
        company: 'Ferrari',
        price: 50.00,
        thumbnail_url: 'https://fotos.jornaldocarro.estadao.com.br/wp-content/uploads/2020/09/30094840/Ferrari-Omologata-one-off-1.jpg',
        name: 'joaquim'
    },
    {
        category: 'esportivos',
        id: 9,
        thumbnail: 'foto-1.png',
        company: 'Lamborguini',
        price: 50.00,
        thumbnail_url: 'https://upload.wikimedia.org/wikipedia/commons/1/18/Lamborghini_Sian_at_IAA_2019_IMG_0332.jpg',
        name: 'joaquim'
    },    
    {
        category: 'esportivos',
        id: 10,
        thumbnail: 'foto-1.png',
        company: 'Lamborguini',
        price: 50.00,
        thumbnail_url: 'https://fotos.jornaldocarro.estadao.com.br/wp-content/uploads/2020/10/05074928/Lamborghini-Huracan-frente2-1160x585.jpg',
        name: 'joaquim'
    },
    {
        category: 'luxo',
        id: 11,
        thumbnail: 'foto-1.png',
        company: 'Audi',
        price: 50.00,
        thumbnail_url: 'https://i2.wp.com/loucosporcarro.com.br/wp-content/uploads/2017/01/especial-sedas.jpg?resize=800%2C450&ssl=1',
        name: 'joaquim'
    },
    {
        category: 'luxo',
        id: 12,
        thumbnail: 'foto-1.png',
        company: 'Masseratti',
        price: 50.00,
        thumbnail_url: 'https://mundofixa.com/wp-content/uploads/2017/12/1367_fac18fc74b175a903f40ba9d4239447c.jpg',
        name: 'joaquim'
    },
    {
        category: 'luxo',
        id: 13,
        thumbnail: 'foto-1.png',
        company: 'Jaguar',
        price: 50.00,
        thumbnail_url: 'https://i2.wp.com/top10mais.org/wp-content/uploads/2017/04/Jaguar-XJ-X351-entre-os-carros-sedan-de-luxo-mais-caros-do-mundo.jpg?resize=600%2C335&ssl=1',
        name: 'joaquim'
    },
    {
        category: 'luxo',
        id: 14,
        thumbnail: 'foto-1.png',
        company: 'Mercedez',
        price: 50.00,
        thumbnail_url: 'https://qcveiculos.com.br/wp-content/uploads/2016/01/sedans-de-luxo-3.jpg',
        name: 'joaquim'
    },    
    {
        category: 'SUVs',
        id: 15,
        thumbnail: 'foto-1.png',
        company: 'Jeep',
        price: 50.00,
        thumbnail_url: 'https://fotos.jornaldocarro.estadao.com.br/uploads/2019/06/08140322/45167725-1160x773.jpg',
        name: 'joaquim'
    },
    {
        category: 'SUVs',
        id: 16,
        thumbnail: 'foto-1.png',
        company: 'Land Hover',
        price: 50.00,
        thumbnail_url: 'https://www.autoo.com.br/fotos/2017/9/960_720/land-rover_discovery-sport_2017_1_01092017_7018_960_720.jpg',
        name: 'joaquim'
    },
    {
        category: 'SUVs',
        id: 17,
        thumbnail: 'foto-1.png',
        company: 'Hyundai',
        price: 50.00,
        thumbnail_url: 'https://s3-sa-east-1.amazonaws.com/revista.mobiauto/ImagensSUVs/10-SUVs-mais-vendidos-no-Brasil-em-2019-Hyundai-Creta.jpg',
        name: 'joaquim'
    },
    {
        category: 'SUVs',
        id: 18,
        thumbnail: 'foto-1.png',
        company: 'Nissan',
        price: 50.00,
        thumbnail_url: 'https://i.pinimg.com/originals/c9/41/2a/c9412af5f72c3aa67921ef06ae79a94e.jpg',
        name: 'joaquim'
    },
    
    
]

export const Categories = [
    'populares',
    'esportivos',
    'luxo',
    'SUVs'
]
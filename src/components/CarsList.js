import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Cars } from '../data'

const CarsList = ({ category }) => { 
  const [ cars, setCars ] = useState([])
  useEffect(() => {
    function loadSpots(category_param) {
      const rs_cars = Cars.filter(el => {
        if(el.category.includes(category_param) > 0) {
          return el
        }
      })

      setCars(rs_cars)
    }

    loadSpots(category)
  }, []) 

  return (
    <View>
      <Text style={ styles.titulo }>Carros na categoria <Text style={ styles.bold }>{ category }</Text></Text>
      <FlatList
      style={ styles.list }
      data={ cars }
      keyExtractor={ car => car.id.toString() }
      horizontal
      showsHorizontalScrollIndicator={ false }
      renderItem={({ item }) => (
        <View style={ styles.listItem }>
          <Image style={ styles.thumbnail }  source={{ uri: item.thumbnail_url }} />
          <Text style={ styles.company }>{ item.company }</Text>
          <Text style={ styles.price }>{ item.price ? `R$ ${item.price}/dia` : 'GRATUITO' }</Text>
          <TouchableOpacity style={ styles.button }>
            <Text style={ styles.buttonText } onPress={ () => {} }>Solicitar reserva</Text>
          </TouchableOpacity>
        </View>
      )} />
    </View>    
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 30
  },
  titulo: {
    marginTop: 10,    
    fontSize: 20,
    color: '#444',
    paddingHorizontal: 20,    
    marginBottom: 10
  },
  bold: {
    fontWeight: 'bold'
  },
  list: {
    paddingHorizontal: 20    
  },
  listItem: {
    marginRight: 15
  },
  thumbnail: {
    width: 200,
    height: 120,
    resizeMode: 'contain',
    borderRadius: 2
  },
  company: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#333',
    marginTop: 10
  },
  price: {
    fontSize: 15,
    color: '#999',
    // marginTop: 5
  },
  button: {
    height: 32,
    backgroundColor: '#f05a5b',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
    marginTop: 15
  },
  buttonText: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 15
  }
})

export default CarsList;